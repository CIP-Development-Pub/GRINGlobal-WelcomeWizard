﻿namespace HelloWizard
{
    partial class WelcomeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_message = new System.Windows.Forms.Label();
            this.groupBox_Accession = new System.Windows.Forms.GroupBox();
            this.comboBox_AccessionStatus = new System.Windows.Forms.ComboBox();
            this.textBox_AccessionNumber = new System.Windows.Forms.TextBox();
            this.textBox_AccessionId = new System.Windows.Forms.TextBox();
            this.label_Status = new System.Windows.Forms.Label();
            this.label_Number = new System.Windows.Forms.Label();
            this.label_ID = new System.Windows.Forms.Label();
            this.button_Save = new System.Windows.Forms.Button();
            this.button_GetLocalData = new System.Windows.Forms.Button();
            this.button_GetWebServiceData = new System.Windows.Forms.Button();
            this.groupBox_Accession.SuspendLayout();
            this.SuspendLayout();
            // 
            // label_message
            // 
            this.label_message.AutoSize = true;
            this.label_message.Location = new System.Drawing.Point(12, 9);
            this.label_message.Name = "label_message";
            this.label_message.Size = new System.Drawing.Size(31, 13);
            this.label_message.TabIndex = 0;
            this.label_message.Text = "Hello";
            // 
            // groupBox_Accession
            // 
            this.groupBox_Accession.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox_Accession.Controls.Add(this.comboBox_AccessionStatus);
            this.groupBox_Accession.Controls.Add(this.textBox_AccessionNumber);
            this.groupBox_Accession.Controls.Add(this.textBox_AccessionId);
            this.groupBox_Accession.Controls.Add(this.label_Status);
            this.groupBox_Accession.Controls.Add(this.label_Number);
            this.groupBox_Accession.Controls.Add(this.label_ID);
            this.groupBox_Accession.Location = new System.Drawing.Point(12, 37);
            this.groupBox_Accession.Name = "groupBox_Accession";
            this.groupBox_Accession.Size = new System.Drawing.Size(519, 108);
            this.groupBox_Accession.TabIndex = 1;
            this.groupBox_Accession.TabStop = false;
            this.groupBox_Accession.Text = "Accession Info";
            // 
            // comboBox_AccessionStatus
            // 
            this.comboBox_AccessionStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_AccessionStatus.FormattingEnabled = true;
            this.comboBox_AccessionStatus.Location = new System.Drawing.Point(197, 69);
            this.comboBox_AccessionStatus.Name = "comboBox_AccessionStatus";
            this.comboBox_AccessionStatus.Size = new System.Drawing.Size(311, 21);
            this.comboBox_AccessionStatus.TabIndex = 4;
            // 
            // textBox_AccessionNumber
            // 
            this.textBox_AccessionNumber.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_AccessionNumber.Location = new System.Drawing.Point(197, 44);
            this.textBox_AccessionNumber.Name = "textBox_AccessionNumber";
            this.textBox_AccessionNumber.ReadOnly = true;
            this.textBox_AccessionNumber.Size = new System.Drawing.Size(311, 20);
            this.textBox_AccessionNumber.TabIndex = 3;
            // 
            // textBox_AccessionId
            // 
            this.textBox_AccessionId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_AccessionId.Location = new System.Drawing.Point(197, 18);
            this.textBox_AccessionId.Name = "textBox_AccessionId";
            this.textBox_AccessionId.ReadOnly = true;
            this.textBox_AccessionId.Size = new System.Drawing.Size(311, 20);
            this.textBox_AccessionId.TabIndex = 3;
            // 
            // label_Status
            // 
            this.label_Status.AutoSize = true;
            this.label_Status.Location = new System.Drawing.Point(16, 72);
            this.label_Status.Name = "label_Status";
            this.label_Status.Size = new System.Drawing.Size(89, 13);
            this.label_Status.TabIndex = 2;
            this.label_Status.Text = "Accession Status";
            // 
            // label_Number
            // 
            this.label_Number.AutoSize = true;
            this.label_Number.Location = new System.Drawing.Point(16, 49);
            this.label_Number.Name = "label_Number";
            this.label_Number.Size = new System.Drawing.Size(96, 13);
            this.label_Number.TabIndex = 1;
            this.label_Number.Text = "Accession Number";
            // 
            // label_ID
            // 
            this.label_ID.AutoSize = true;
            this.label_ID.Location = new System.Drawing.Point(16, 25);
            this.label_ID.Name = "label_ID";
            this.label_ID.Size = new System.Drawing.Size(70, 13);
            this.label_ID.TabIndex = 0;
            this.label_ID.Text = "Accession ID";
            // 
            // button_Save
            // 
            this.button_Save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_Save.Location = new System.Drawing.Point(368, 190);
            this.button_Save.Name = "button_Save";
            this.button_Save.Size = new System.Drawing.Size(163, 23);
            this.button_Save.TabIndex = 2;
            this.button_Save.Text = "Save";
            this.button_Save.UseVisualStyleBackColor = true;
            this.button_Save.Click += new System.EventHandler(this.button_Save_Click);
            // 
            // button_GetLocalData
            // 
            this.button_GetLocalData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_GetLocalData.Location = new System.Drawing.Point(30, 190);
            this.button_GetLocalData.Name = "button_GetLocalData";
            this.button_GetLocalData.Size = new System.Drawing.Size(163, 23);
            this.button_GetLocalData.TabIndex = 3;
            this.button_GetLocalData.Text = "Get from Local Database";
            this.button_GetLocalData.UseVisualStyleBackColor = true;
            this.button_GetLocalData.Click += new System.EventHandler(this.button_GetLocalData_Click);
            // 
            // button_GetWebServiceData
            // 
            this.button_GetWebServiceData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_GetWebServiceData.Location = new System.Drawing.Point(199, 190);
            this.button_GetWebServiceData.Name = "button_GetWebServiceData";
            this.button_GetWebServiceData.Size = new System.Drawing.Size(163, 23);
            this.button_GetWebServiceData.TabIndex = 4;
            this.button_GetWebServiceData.Text = "Get from GRIN-Global Server";
            this.button_GetWebServiceData.UseVisualStyleBackColor = true;
            this.button_GetWebServiceData.Click += new System.EventHandler(this.button_GetWebServiceData_Click);
            // 
            // WelcomeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(543, 228);
            this.Controls.Add(this.button_GetLocalData);
            this.Controls.Add(this.button_GetWebServiceData);
            this.Controls.Add(this.button_Save);
            this.Controls.Add(this.groupBox_Accession);
            this.Controls.Add(this.label_message);
            this.Name = "WelcomeForm";
            this.Text = "Welcome Wizard";
            this.Load += new System.EventHandler(this.WelcomeForm_Load);
            this.groupBox_Accession.ResumeLayout(false);
            this.groupBox_Accession.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_message;
        private System.Windows.Forms.GroupBox groupBox_Accession;
        private System.Windows.Forms.Label label_Status;
        private System.Windows.Forms.Label label_Number;
        private System.Windows.Forms.Label label_ID;
        private System.Windows.Forms.Button button_Save;
        private System.Windows.Forms.ComboBox comboBox_AccessionStatus;
        private System.Windows.Forms.TextBox textBox_AccessionNumber;
        private System.Windows.Forms.TextBox textBox_AccessionId;
        private System.Windows.Forms.Button button_GetLocalData;
        private System.Windows.Forms.Button button_GetWebServiceData;
    }
}